package clases;

public class Notas {
	private int nota;
	private Alumno alumno;
	private Asignaturas asignatura;
	
	//Constructores
	public Notas() {
		
	}
	
	public Notas(int nota, Alumno alumno, Asignaturas asignatura) {
		this.nota = nota;
		this.alumno = alumno;
		this.asignatura = asignatura;
	}
	
	public Notas(Notas original) {
		this.nota = original.nota;
		this.alumno = original.alumno;
		this.asignatura = original.asignatura;
	}

	//Setters y Getters
	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Asignaturas getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(Asignaturas asignatura) {
		this.asignatura = asignatura;
	}
	
	
}
