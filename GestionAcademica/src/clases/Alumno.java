package clases;

public class Alumno {
	private String nombre;
	private String apellido;
	private String dni;
	
	//Constructores
	public Alumno() {
		
	}
	
	public Alumno(String nombre, String apellido, String dni) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
	}
	
	public Alumno(Alumno original) {
		this.nombre = original.nombre;
		this.apellido = original.apellido;
		this.dni = original.dni;
	}

	//Setters y Getters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	
	
}
