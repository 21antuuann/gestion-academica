package clases;

public class Asignaturas {
	private String nombre;
	private int codigo;
	
	//Constructores
	public Asignaturas() {
		
	}
	
	public Asignaturas(String nombre, int codigo) {
		this.nombre = nombre;
		this.codigo = codigo;
	}
	
	public Asignaturas(Asignaturas original) {
		this.nombre = original.nombre;
		this.codigo = original.codigo;
	}

	//Setters y Getters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
}
