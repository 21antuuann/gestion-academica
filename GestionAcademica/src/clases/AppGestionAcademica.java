package clases;

import java.util.ArrayList;

import daw.com.Pantalla;
import daw.com.Teclado;

public class AppGestionAcademica {

	public static void main(String[] args) {
		// TODO Esbozo de método generado automáticamente
		ArrayList<Alumno> listaAlumnos = new ArrayList<Alumno>();
		int opc=0, SALIR=5;
		
		while (opc != SALIR) {
			Pantalla.escribirString("Seleccione una opcion:");
			Pantalla.escribirString("\n1. Alta de alumno");
			Pantalla.escribirString("\n2. Baja de alumno");
			Pantalla.escribirString("\n3. Consulta de alumno");
			Pantalla.escribirString("\n4. Modificacion de alumno");
			Pantalla.escribirString("\n5. Salir");
			opc = Teclado.leerInt("\n--> ");
			
			switch (opc) {
			case 1:
				altaAlumno(listaAlumnos);
				break;
			case 2:
				bajaAlumno(listaAlumnos);
				break;
			case 3:
				consultaAlumno(listaAlumnos);
				break;
			case 4:
				modificacionAlumno(listaAlumnos);
				break;
			case 5:
				Pantalla.escribirString("\nSaliendo del programa...");
				break;
			default:
				Pantalla.escribirString("Opcion incorrecta. Intente de nuevo. ");
				break;
			}		
		}
	}

	private static void modificacionAlumno(ArrayList<Alumno> listaAlumnos) {
		// TODO Esbozo de método generado automáticamente
		String dni = Teclado.leerString("Ingrese el DNI del alumno a modificar: ");
		
		boolean encontrado = false;
		
		for (int i = 0; i < listaAlumnos.size(); i++) {
			Alumno alumno = listaAlumnos.get(i);
			
			if (alumno.getDni() == dni) {
				String nuevoNombre = Teclado.leerString("Ingrese el nuevo nombre: ");
				alumno.setNombre(nuevoNombre);
				String nuevoApellido = Teclado.leerString("Ingrese el nuevo apellido: ");
				alumno.setApellido(nuevoApellido);
				String nuevoDNI = Teclado.leerString("Ingrese el nuevo DNI: ");
				alumno.setDni(nuevoDNI);
				
				encontrado = true;
				Pantalla.escribirString("\nAlumno modificado correctamente.");
				Pantalla.escribirSaltoLinea();
				break;
			}
		}
		if (!encontrado) {
			Pantalla.escribirString("\nNo se ha encontrado al alumno con ese DNI.");
			Pantalla.escribirSaltoLinea();
		}
	}

	private static void consultaAlumno(ArrayList<Alumno> listaAlumnos) {
		// TODO Esbozo de método generado automáticamente
		String dni = Teclado.leerString("Ingrese el DNI del alumno a consultar: ");
		
		boolean encontrado = false;
		
		for (int i = 0; i < listaAlumnos.size(); i++) {
			Alumno alumno = listaAlumnos.get(i);
			
			if (alumno.getDni() == dni) {
				Pantalla.escribirString("\nNombre: " + alumno.getNombre());
				Pantalla.escribirString("\nApellido: " + alumno.getApellido());
				Pantalla.escribirString("\nDNI: " + alumno.getDni());
				Pantalla.escribirSaltoLinea();
				encontrado = true;
				break;
			}
		}
		if (!encontrado) {
			Pantalla.escribirString("\nNo se ha encontrado al alumno con ese DNI.");
			Pantalla.escribirSaltoLinea();
		}
	}

	private static void bajaAlumno(ArrayList<Alumno> listaAlumnos) {
		// TODO Esbozo de método generado automáticamente
		String dni = Teclado.leerString("Ingrese el DNI del alumno a dar de baja: ");
		
		boolean encontrado = false;
		
		for (int i = 0;  i < listaAlumnos.size(); i++) {
			Alumno alumno = listaAlumnos.get(i);
			
			if (alumno.getDni() == dni) {
				listaAlumnos.remove(i);
				encontrado = true;
				Pantalla.escribirString("\nAlumno eliminado correctamente.");
				Pantalla.escribirSaltoLinea();
				break;
			}
		}
		if (!encontrado) {
			Pantalla.escribirString("\nNo hay ningun alumno con ese DNI.");
			Pantalla.escribirSaltoLinea();
		}
	}

	private static void altaAlumno(ArrayList<Alumno> listaAlumnos) {
		// TODO Esbozo de método generado automáticamente
		String nombre = Teclado.leerString("Nombre del alumno: ");
		String apellido = Teclado.leerString("Apellido del alumno: ");
		String dni = Teclado.leerString("DNI del alumno: ");
		
		Alumno nuevoAlumno = new Alumno(nombre, apellido, dni);
		listaAlumnos.add(nuevoAlumno);
		
		Pantalla.escribirString("\nAlumno agregado correctamente.");
		Pantalla.escribirSaltoLinea();
	}
	
	

}
