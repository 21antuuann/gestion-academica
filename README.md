## Nombre
Este es un proyecto para el modulo de Entornos de Desarrollo, creado por Antonio Monreal Diaz.

## Descripcion
Este proyecto consiste en crear una aplicacion en Java en la cual se puedan gestionar datos academicos. Para ello tenemos creadas las clases Alumno, Asignatura, Cursos y Notas. A parte esta la aplicacion, en la que se muestra un menu con opciones y podemos realizar distintas acciones.

## Instalación
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

Para poder probar la aplicacion se requiere tener instalado algun software IDE en el que se pueda manipular archivos JAVA, en este caso, yo lo he echo con Eclipse. Simplemente habría que copiar la carpeta "GestionAcademica" al workspace de Eclipse.

## Estado del proyecto
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

Actualmente, el proyecto se encuentra en una fase de prueba, en las que las clases y la aplicación puede que no funcionen correctamente. A medida que vaya avanzando el curso, se iran añadiendo nuevas funcionalidades y se iran reparando errores.
